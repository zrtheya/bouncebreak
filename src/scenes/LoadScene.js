class LoadScene extends Phaser.Scene{
    constructor() {
        super("LOAD");
    }

    init() {
        //to prepare data
    }

    preload() {
        //used to load the music and images into memory
        this.add.text(300,250, "Loading game . . .")
        this.load.image("loadingbg", "src/globals/assets/loadingbg.jpg");
        this.load.image("start", "src/globals/assets/start.png");

        this.load.spritesheet("cat", "src/globals/assets/cat.png", {
            frameHeight: 32,
            frameWidth: 32
        });

        let loadingBar = this.add.graphics({
            fillStyle: {
                color: 0xF0E68C
            }
        });


        //simulate large load to 100
        for (let i = 0; i < 100; i++) {
            this.load.spritesheet("cat" + i, "src/globals/assets/cat.png", {
                frameHeight: 32,
                frameWidth: 32
            });
        }
        this.load.on("progress", (percent) => {
            loadingBar.fillRect(0, (this.game.renderer.height / 2) , this.game.renderer.width * percent, 50); // position of the loading bar and ist height
            console.log(percent);
        });

        this.load.on("complete", () => {
            console.log("done")
        })
    
    }
    

    create() {
        // used to add the objects to the game
        
        // this.initGlobalVariables()
        this.scene.start("MENU");
        console.log("transfers load to the menu scene");
        
    }
    // initGlobalVariables() {
    //     this.game.global = clone(globals)
    // }
} 