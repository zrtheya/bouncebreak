class MenuScene extends Phaser.Scene{
    constructor() {
        super("MENU");
    }

    init(data) {
        console.log(data)
        console.log("received the data from the load scene")

    }

    create() {
        // create images in z order
        this.add.image(0,0, "loadingbg").setOrigin(0).setDepth(0);
        let start = this.add.image(this.game.renderer.width / 2 , this.game.renderer.height / 2 - 100, "start");
        start.setScale(.35)
    
        // create sprites
        let hoverSprite = this.add.sprite(100, 100, "cat");
        hoverSprite.setScale(2);
        // hoverSprite.setVisible(false);
        start.setInteractive();

        // hovering
        start.on("pointerover", () => {
            hoverSprite.setVisible(true);
            hoverSprite.x = start.x - start.width;
            hoverSprite.y = start.y - 100;
            console.log("hover")
        })
        start.on("pointerout", () => {
            hoverSprite.setVisible(true);
            console.log("hoverout")
        })
        start.on("pointerdown",() => {
            this.scene.start("GAME")
            console.log("clicked")
        })
    
    }
}